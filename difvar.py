import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cmx
import matplotlib
from matplotlib.colors import Normalize
from scipy.stats import ttest_ind_from_stats
from collections import Counter

def pandasprocessing(axes, value,df, limiter=None):
    if limiter != None:
        df = df.loc[df[limiter[0]] == limiter[1]]
    else:
        pass
    dfcomp = df.set_index(axes[1])
    df = df.set_index(axes)
    if len(axes) == 2:
        xaxis_name = axes[0]
        yaxis_name = axes[1]
        #df = df[[xaxis_name, yaxis_name, value]]
        #df[xaxis_name].fillna(df[xaxis_name].mean())
        #df[yaxis_name].fillna(df[yaxis_name].mean())

        #xaxis = df[xaxis_name].sort_values(ascending=True)
        xaxis = df.index.levels[0].values
        #yaxis = df[yaxis_name].sort_values(ascending=False)
        yaxis = df.index.levels[1].values
        #print df[value]
        #print xaxis, yaxis
        #df = df.set_index([xaxis_name, yaxis_name])
        #print df
        pixel_array = np.zeros([len(xaxis),len(yaxis)])
        pval_array = np.zeros([len(xaxis), len(yaxis)])
        #sudo = df.loc[df[xaxis_name] == 585.0]
        #print sudo
        #val = sudo.loc[sudo[yaxis_name] == 648.0]['avg']
        #print val.values[0]
        #print np.shape(pixel_array)
        #df = df.set_index([xaxis_name, yaxis_name])
        counts = {}
        #print df
        for i1, x in enumerate(xaxis):
            #print i1
            for i2, y in enumerate(yaxis):
                try:
                    #print df.loc[x,y][value]
                    #print df.loc['White',y][value]
                    mean = df.loc[x,y][value].mean()
                    std = df.loc[x,y][value].std()
                    n = len(df.loc[x,y][value])
                    #mean2 = df.loc['White',y][value].mean()
                    #std2 = df.loc['White',y][value].std()
                    #n2 = len(df.loc['White',y][value])


                    #mean2 = dfcomp.loc[y][value].mean()
                    #std2 = dfcomp.loc[y][value].std()
                    #n2 = len(dfcomp.loc[y][value])
                    #print "grade {}: ".format(y), mean2, std2, n2
                    #print(mean2,std2,n2)
                    #print df.loc[x,y][value].describe()
                    val = round(mean,1)
                    #tstat, pvalue = ttest_ind_from_stats(mean, std, n, mean2, std2, n2)
                    #print pvalue

                    #pvalue = round(pvalue, 3)
                    pvalue = 0.0
                    #if x in counts:
                    #    counts[x] += n
                    #else:
                    #    counts[x] = n
                    try:
                        pixel_array[i1][i2] = val
                        pval_array[i1][i2] = pvalue
                    except ValueError:
                        pixel_array[i1][i2] = val.values[0]
                        pval_array[i1][i2] = pvalue
                except KeyError:
                    val = np.nan
                    pixel_array[i1][i2] = val

                #except KeyError:
                #    val = np.nan
                #    pixel_array[i1][i2] = val"""
        #print pixel_array, pval_array
        #print counts
        return {"data" : pixel_array,
                "pval" : pval_array,
                "xaxis": xaxis,
                "yaxis": yaxis,
                "x_name": xaxis_name,
                "y_name": yaxis_name,
                "count": counts}

def produceimage(tommysucks, dim="2D"):
    class MidPointNormalize(Normalize):
        def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
            self.midpoint = midpoint
            Normalize.__init__(self, vmin, vmax, clip)

        def __call__(self, value, clip=None):
            # I'm ignoring masked values and all kinds of edge cases to make a
            # simple example...
            x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
            return np.ma.masked_array(np.interp(value, x, y))
    norm = MidPointNormalize(midpoint = 0)
    if dim == "2D":
        #plt.imshow(tommysucks['data'],interpolation="none", cmap='seismic',extent=[np.min(tommysucks['xaxis']), np.max(tommysucks['xaxis']), np.min(tommysucks['yaxis']), np.max(tommysucks['yaxis'])], aspect='auto')
        plt.imshow(tommysucks['data'],interpolation="none", cmap='seismic', aspect='auto')

        #plt.scatter(tommysucks['xaxis'], tommysucks['yaxis'], cmap='seismic')
        plt.xlabel(tommysucks['x_name'])
        plt.ylabel(tommysucks['y_name'])
        plt.colorbar()
        plt.show()
    elif dim == "categorical":
        fig, ax = plt.subplots()
        im = ax.imshow(tommysucks['data'],cmap='seismic',aspect='auto')

        # We want to show all ticks...
        ax.set_xticks(np.arange(len(tommysucks['yaxis'])))
        ax.set_yticks(np.arange(len(tommysucks['xaxis'])))
        # ... and label them with the respective list entries
        ax.set_xticklabels(tommysucks['yaxis'])
        ax.set_yticklabels(tommysucks['xaxis'])

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

         # Loop over data dimensions and create text annotations.
        for i in range(len(tommysucks['xaxis'])):
            for j in range(len(tommysucks['yaxis'])):
                text = ax.text(j, i, "{} \n {}".format(tommysucks['data'][i, j], tommysucks['pval'][i, j]),
                ha="center", va="center", color="w")

        fig.suptitle("Average Test Scores")
        ax.set_title(" ")
        fig.tight_layout()
        cbar = ax.figure.colorbar(im, ax=ax)
        cbar.ax.set_ylabel("", rotation=-90, va="bottom")
        plt.show()

def bar_chart(axes, trumppercent, df):
    df = df.set_index(axes)

    df = df['math_scale_score'].groupby(level=['school_county','race', 'grade']).mean().to_frame()
    xaxis = df.index.levels[0].values
    yaxis = df.index.levels[1].values
    zaxis = df.index.levels[2].values

    print(type(df))
    print(df)
    for x in xaxis:
        for y in yaxis:
            for z in zaxis:

        #df['percent'] =
                try:
                    df.loc[x,'percent'] = trumppercent[x]
                except TypeError:
                    print x,y,z, type(x), type(y), type(z)
                except KeyError:
                    pass
    df.to_csv('csvs/black_bycounty.csv')
    #print df


if __name__ == "__main__":
    trumppercent = {
    "ADAMS" : .7371,
    "ALLEN" : .5704,
    "BARTHOLOMEW" : .6353,
    "BENTON" : .7035,
    "BLACKFORD" : .6923,
    "BOONE" : .6108,
    "BROWN" : .6313,
    "CARROLL" : .7258,
    "CASS" : .6827,
    "CLARK" : .5872,
    "CLAY" : .7562,
    "CLINTON" : .7147,
    "CRAWFORD" : .6564,
    "DAVIESS" : .7936,
    "DEARBORN" : .7551,
    "DECATUR" : .7630,
    "DEKALB" : .7132,
    "DELAWARE" : .5377,
    "DUBOIS" : .6688,
    "ELKHART" : .6379,
    "FAYETTE" : .7176,
    "FLOYD" : .5727,
    "FOUNTAIN" : .7557,
    "FRANKLIN" : .7872,
    "FULTON" : .7177,
    "GIBSON" : .7156,
    "GRANT" : .6719,
    "GREENE" : .7447,
    "HAMILTON" : .5666,
    "HANCOCK" : .6930,
    "HARRISON" : .6974,
    "HENDRICKS" : .6397,
    "HENRY" : .6894,
    "HOWARD" : .6426,
    "HUNTINGTON" : .7261,
    "JACKSON" : .7317,
    "JASPER" : .7013,
    "JAY" : .7134,
    "JEFFERSON" : .6310,
    "JENNINGS" : .7376,
    "JOHNSON" : .6835,
    "KNOX" : .7139,
    "KOSCIUSKO" : .7460,
    "LAGRANGE" : .5019,
    "LAPORTE" : .7344,
    "LAKE" : .3759,
    "LAWRENCE" : .7328,
    "MADISON" : .6006,
    "MARION" : .3597,
    "MARSHALL" : .6796,
    "MARTIN" : .7688,
    "MIAMI" : .7399,
    "MONROE" : .3523,
    "MONTGOMERY" : .7297,
    "MORGAN" : .7568,
    "NEWTON" : .7035,
    "NOBLE" : .7222,
    "OHIO" : .7251,
    "ORANGE" : .7084,
    "OWEN" : .7191,
    "PARKE" : .7388,
    "PERRY" : .5656,
    "PIKE" : .7358,
    "PORTER" : .5031,
    "POSEY" : .6741,
    "PULASKI" : .7060,
    "PUTNAM" : .7229,
    "RANDOLPH" : .7188,
    "RIPLEY" : .7655,
    "RUSH" : .7336,
    "SCOTT" : .6674,
    "SHELBY" : .7068,
    "SPENCER" : .6586,
    "ST. JOSEPH" : .4727,
    "STARKE" : .6901,
    "STEUBEN" : .6957,
    "SULLIVAN" : .7171,
    "SWITZERLAND" : .6915,
    "TIPPECANOE" : .4922,
    "TIPTON" : .7442,
    "UNION" : .7429,
    "VANDERBURGH" : .5585,
    "VERMILLION" : .6504,
    "VIGO" : .5509,
    "WABASH" : .7292,
    "WARREN" : .7374,
    "WARRICK" : .6459,
    "WASHINGTON" : .7212,
    "WAYNE" : .6266,
    "WELLS" : .7588,
    "WHITE" : .6857,
    "WHITLEY" : .7270
    }
    count = []
    for key, val in trumppercent.items():
        if val in count:
            print(key, val)
            break
        count.append(val)

    #trump50_under = {}
    #trump50_to_60 = {}
    #trump60_to_70 = {}
    #trump70_up = {}

    """
    trumppercent = sorted(trumppercent.items(), key=lambda t:t[1], reverse=False)
    nums = len(trumppercent)
    fourthq = [i[0] for i in trumppercent[0:nums//4]]
    thirdq = [i[0] for i in trumppercent[nums//4:nums//2]]
    secondq = [i[0] for i in trumppercent[nums//2: 3*nums//4]]
    firstq = [i[0] for i in trumppercent[3*nums//4: nums]]
    print firstq
    #del trumppercent
    #print fourthq
    #print trump50_under
    #print trump50_to_60
    #print trump60_to_70
    #print trump70_up
    """
    filename = '/Users/patricklink/Downloads/IDOE_Combined_180611.dta'
    reader = pd.read_stata(filename, chunksize=100000)
    lines = 0
    #print("ADAMS" in trump50_under)
    #dffirstq = pd.DataFrame(columns=['school_county','math_scale_score','race','grade'])
    #dfsecondq = pd.DataFrame(columns=['school_county','math_scale_score','race','grade'])
    #dfthirdq = pd.DataFrame(columns=['school_county','math_scale_score','race','grade'])
    #dffourthq = pd.DataFrame(columns=['school_county','math_scale_score','race','grade', "male", "school_class"])
    df = pd.DataFrame(columns=['school_county', 'math_scale_score', 'race', 'grade'])
    #dffrl = pd.DataFrame(columns=['school_county','math_scale_score','race','grade'])
    #dfpaid = pd.DataFrame(columns=['school_county','math_scale_score','race','grade'])
    #df_race_county = pd.DataFrame(columns=['school_county','math_scale_score','race'])
    for chunk in reader:
        #print chunk['school_county']
        #chunk = chunk.loc[chunk['grade'] == 5]

        chunk = chunk.loc[chunk['school_county'] != '']
        chunk = chunk.loc[chunk['school_county'] != 'M']
        #chunk.school_county = chunk.school_county.apply(lambda x: 1.0 if x in firstq else x)
        #chunk.school_county = chunk.school_county.apply(lambda x: 2.0 if x in secondq else x)
        #chunk.school_county = chunk.school_county.apply(lambda x: 3.0 if x in thirdq else x)
        #chunk.school_county = chunk.school_county.apply(lambda x: 4.0 if x in fourthq else x)
        chunk = chunk.loc[chunk['race'] == 'Black']
        chunk = chunk.loc[chunk['grade'] >=5]
        #chunk = chunk.loc[~chunk['race'].isin(('Black', 'White'))]
        #df.loc[df['column_name'].isin(some_values)]
        #print(chunk)
        #chunk['Black'] = chunk['Black'] - chunk['White']

        #print(chunk)
        #chunk = chunk.loc[chunk['school_county'] == 1.0]
        #print chunk.groupby('school_class').count()
        #print chunk.groupby('school_class').count()['grade']["Traditional Public"]

        chunk = chunk[['school_county','math_scale_score','race','grade']]

        #frl = chunk.loc[chunk['frl'] == "Free/Reduced Lunch"]
        #paid = chunk.loc[chunk['frl'] == "Paid Lunch"]
        lines += 100000
        #if lines > 100000:
        #    break
        print lines

        #chunk = chunk.loc[chunk['school_county'].isin(fourthq)]

        #print frl.head
        #print paid.head
        #chunk = chunk[['school_county','math_scale_score','race']]

        df = df.append(chunk)
        #dffrl = dffrl.append(frl)
        #dfpaid = dfpaid.append(paid)
        #df_race_county = df_race_county.append(chunk)

    bar_chart(['school_county', 'race', 'grade'], trumppercent, df)
    #dffourthq.dropna(axis=0, subset=["male"], inplace=True)
    #print(dffourthq)
    #dffirstq = dffirstq.set_index(['race','grade'])
    #dfsecondq = dfsecondq.set_index(['race','grade'])
    #dfthirdq = dfthirdq.set_index(['race','grade'])
    #print len(df60_to_70)
    #dffourthq = dffourthq.set_index(['race','grade'])
    #dffrl = dffrl.set_index(['race', 'grade'])
    #dfpaid = dfpaid.set_index(['race', 'grade'])
    #df_race_county = df_race_county.set_index(['race', 'school_county'])


    #tommysucks = pandasprocessing(['school_county', "grade"],"math_scale_score", df=dffourthq)


    #print tommysucks['data']
    #with open('PaidLunchbyQuart.csv', 'a') as f:
    #    [f.write('{0},{1}\n'.format(key, value)) for key, value in tommysucks['count'].items()]


    #produceimage(tommysucks, dim="categorical")


    #print lines
    """6,031,000 lines in dataset"""
    #chunk1 = reader.get_chunk(10000)


    #print chunk1['school_county']
    #chunk1['avg'] = chunk1[['ela_scale_score', 'math_scale_score']].mean(axis=1)
    #print chunk1['idoe_black_school_enroll']

    #tommysucks = pandasprocessing(('race', "grade"),"avg", df=race)
    #print tommysucks['data']
    #produceimage(tommysucks, dim="categorical")
    #print tommysucks
    #with open('columns.csv','w') as f:
    #    for column in chunk1.columns:
    #        f.write("{}\n".format(column))
